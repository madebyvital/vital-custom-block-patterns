# Vital Block Patterns

**DEPRECATED - with WP core as of 6.3 having block patterns now, this is no longer needed.**

Manage Block Patterns with a custom post type like Reusable Blocks.


> NOTE: As of WordPress version 6.3 the `wp_block` post type can be either a synced (default) or unsynced pattern.