<?php

namespace Vital;

defined('ABSPATH') || exit;
/*
    Plugin Name: Vital Block Patterns
    Plugin URI: https://vtldesign.com
    Description: Manage Block Patterns in a Custom Post Type
    Version: 2.0.9
    Requires at least: 5.5
    Requires PHP: 7.4
    Author: Vital
    Author URI: https://vtldesign.com
    Text Domain: vital
*/

if (!class_exists('\Vital\Custom_Block_Patterns')) {
	class Custom_Block_Patterns {
		/**
		 * @var string
		 */
		const SLUG = 'wp_block';

		/**
		 * @var string
		 */
		const TAXONOMY = 'wp_pattern_category';

		/**
		 * @var string
		 */
		const Block_Pattern_CPT = 'customblockpattern';

		/**
		 * The single instance of the class.
		 *
		 * @var object
		 * @access private
		 * @since 0.1.0
		 */
		private static $instance;

		/**
		 * The path to the main plugin file
		 *
		 * @var string
		 * @access public
		 * @since 0.1.0
		 */
		public $path;

		/**
		 * the URL to the root of the plugin
		 *
		 * @var string
		 * @access public
		 * @since 0.1.0
		 */
		public $url;

		public function __construct() {
			$this->path = \plugin_dir_path(__FILE__);
			$this->url = \plugin_dir_url(__FILE__);
			$this->__add_includes();

			\register_activation_hook(__FILE__, [__CLASS__, 'plugin_activated']);
			\add_action('wp_enqueue_scripts', [__CLASS__, 'maybe_enqueue_scripts']);
		}

		static function maybe_enqueue_scripts() {
			if (!is_post_type_archive(self::SLUG)) {
				return;
			}

			$css_file_path = self::$instance->path . 'build/index.css';
			if (file_exists($css_file_path)) {
				\wp_enqueue_style(
					'vital-custom-block-patterns-styles',
					self::$instance->url . '/build/index.css',
					[],
					filemtime($css_file_path)
				);
			}
		}

		/**
		 * pulls in any local files needed
		 *
		 * @return void
		 */
		protected function __add_includes() {
			require_once($this->path . 'includes/taxonomies/pattern-type.php');
			require_once($this->path . 'includes/taxonomies/pattern-category.php');
			require_once($this->path . 'includes/core-posttype.php');
			require_once($this->path . 'includes/posttype.php');
			require_once $this->path . 'includes/options.php';

			require_once $this->path . 'includes/archive.php';
			require_once $this->path . 'includes/actions.php';
		}

		public static function instance() {
			if (!isset(self::$instance) || !(self::$instance instanceof Custom_Block_Patterns)) {
				self::$instance = new Custom_Block_Patterns;
			}

			return self::$instance;
		}

		/**
		 * performs any necessary actions upon activation of this plugin
		 *
		 * @return void
		 */
		public static function plugin_activated() {
			//flush_rewrite_rules();
		}
	}

	Custom_Block_Patterns::instance();
}

define('VITAL_CUSTOM_BLOCK_PATTERNS_VERSION', '2.0.9');

if (!class_exists('\Skeletor\Plugin_Updater')) {
	require_once(__DIR__ . '/class--plugin-updater.php');
}

$updater = new \Skeletor\Plugin_Updater(
	plugin_basename(__FILE__),
	VITAL_CUSTOM_BLOCK_PATTERNS_VERSION,
	'https://bitbucket.org/madebyvital/vital-custom-block-patterns/raw/HEAD/package.json'
);
