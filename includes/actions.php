<?php

namespace Vital\Custom_Block_Patterns;

use Vital\Custom_Block_Patterns;

defined('ABSPATH') || exit;

class Actions {

	public static function setup() {
		$bulk_actions_filter = sprintf('bulk_actions-edit-%s', Custom_Block_Patterns::Block_Pattern_CPT);
		add_filter($bulk_actions_filter, [__CLASS__, 'add_bulk_actions']);

		$action_handler_filter = sprintf('handle_bulk_actions-edit-%s', Custom_Block_Patterns::Block_Pattern_CPT);
		add_filter($action_handler_filter, [__CLASS__, 'maybe_handle_migration'], 10, 3);

		add_action('admin_notices', [__CLASS__, 'maybe_show_migration_notices']);

		add_action('init', [__CLASS__, 'unregister_custom_block_pattern_cpt']);
		add_action('init', [__CLASS__, 'check_and_redirect_unregistered_post_type']);
	}

	/**
	 * Custom bulk action to migrate the customblockpattern post's content to the
	 * core wp-block / block patterns posttype
	 *
	 * @param string $redirect_to
	 * @param string $action
	 * @param array $post_ids
	 * @return string
	 */
	public static function maybe_handle_migration($redirect_to, $action, $post_ids) {
		if ($action !== 'migrate_patterns') {
			return $redirect_to;
		}

		// remove any query args that indicate success or failure
		$redirect_to = remove_query_arg(
			['vcbp_migration__updated', 'vcbp_migration__errors'],
			$redirect_to
		);

		$successes = [];
		$errors = [];
		foreach ($post_ids as $post_id) {
			if (self::migrate_post($post_id) === true) {
				$successes[] = $post_id;
			} else {
				$errors[] = $post_id;
			}
		}

		// only errors
		if ($errors && !$successes) {
			return add_query_arg('vcbp_migration__errors', implode('|', $errors), $redirect_to);
		}
		// only successes
		if (!$errors && $successes) {
			return add_query_arg('vcbp_migration__updated', implode('|', $successes), $redirect_to);
		}

		return add_query_arg(
			[
				'vcbp_migration__updated' => implode('|', $successes),
				'vcbp_migration__errors'  => implode('|', $errors),
			],
			$redirect_to
		);
	}

	/**
	 * updates the post_type of the give post ID
	 * and adds a post_meta entry to mark as unsynced
	 *
	 * @param int $post_id
	 * @return bool
	 */
	public static function migrate_post($post_id) {
		global $wpdb;

		// update the posttype on the post itself
		$results = $wpdb->update(
			$wpdb->posts,
			[
				'post_type' => 'wp_block',
			],
			[
				'ID'        => $post_id,
				'post_type' => Custom_Block_Patterns::Block_Pattern_CPT,
			],
			[
				'%s',
			],
			[
				'%d',
				'%s',
			]
		);

		// would give back the number of successful updates
		if (!$results) {
			return false;
		}

		// add the 'un-synced' post meta to the post wp_pattern_sync_status = 'unsynced'
		update_post_meta($post_id, 'wp_pattern_sync_status', 'unsynced');
		return true;
	}

	/**
	 * Hook into bulk_actions-edit-customblockpattern to add our own custom actions
	 *
	 * @param array $bulk_actions
	 * @return array
	 */
	public static function add_bulk_actions($bulk_actions) {
		/**
		 * synced patterns were introduced in version 6.3
		 */
		if (version_compare(get_bloginfo('version'), '6.3', '<')) {
			return $bulk_actions;
		}
		$bulk_actions['migrate_patterns'] = __('Migrate to WP Core Patterns');

		return $bulk_actions;
	}

	private static function _show_migration_update_notice() {
		if (empty($_REQUEST['vcbp_migration__updated'])) {
			return;
		}
		$updates_arg = $_REQUEST['vcbp_migration__updated'];
		$updates = explode('|', $updates_arg);
		if (!$updates) {
			return;
		}

		$count = count($updates);
		$message = sprintf(
			'Migrated %d pattern%s.',
			$count,
			$count === 1 ? '' : 's'
		); ?>
		<div class="notice notice-success is-dismissible">
			<p><?php echo $message; ?></p>
		</div>
		<?php
	}

	private static function _show_migration_error_notice() {
		if (empty($_REQUEST['vcbp_migration__errors'])) {
			return;
		}
		$updates_arg = $_REQUEST['vcbp_migration__errors'];
		$updates = explode('|', $updates_arg);
		if (!$updates) {
			return;
		}

		$count = count($updates);
		$message = sprintf(
			'Errors were encountered while migrating %d pattern%s.',
			$count,
			$count === 1 ? '' : 's'
		); ?>
		<div class="notice notice-error is-dismissible">
			<p><?php echo $message; ?></p>
		</div>
		<?php
	}

	public static function maybe_show_migration_notices() {
		self::_show_migration_update_notice();
		self::_show_migration_error_notice();
	}

	public static function unregister_custom_block_pattern_cpt() {
		if (!is_admin()) {
			return;
		}

		$args = array(
			'post_type'      => Custom_Block_Patterns::Block_Pattern_CPT,
			'post_status'    => 'any',
			'posts_per_page' => 1,
		);

		$custom_block_pattern = get_posts($args);

		if (!$custom_block_pattern) {
			unregister_post_type(Custom_Block_Patterns::Block_Pattern_CPT);
		}
	}

	public static function check_and_redirect_unregistered_post_type() {
		global $pagenow;
		if ($pagenow == 'edit.php' && isset($_GET['post_type'])) {
			$post_type = sanitize_text_field($_GET['post_type']);
			$custom_post_types = array(Custom_Block_Patterns::Block_Pattern_CPT);

			if (in_array($post_type, $custom_post_types) && !post_type_exists($post_type)) {
				wp_redirect(admin_url('edit.php?post_type=wp_block'));
				exit;
			}
		}
	}
}

add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Actions', 'setup']);
