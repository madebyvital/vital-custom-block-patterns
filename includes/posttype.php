<?php

namespace Vital\Custom_Block_Patterns;

use Vital\Custom_Block_Patterns;

defined('ABSPATH') || exit;

/**
 * a custom Posttype object for patterns
 * 
 * NOTE: this is 'obsolete' with WP's creation
 * of the `wp_block`. This is only kept so we
 * can convert this items to wp_block posts (if need be)
 */
class Block_Pattern_CPT
{
	const SLUG = 'customblockpattern';

	const CPT_LABELS = [
		'name'          => 'Block Patterns',
		'singular_name' => 'Block Pattern',
		'view_item'     => 'View Block Pattern',
		'view_items'    => 'View Block Patterns',
		'new_item'      => 'New Block Pattern',
		'edit_item'     => 'Edit Block Pattern',
		'add_new_item'  => 'Add New Block Pattern',
		'search_items'  => 'Search Block Patterns'
	];

	const CPT_ARGS = [
		'labels'              => self::CPT_LABELS,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'menu_icon'           => 'dashicons-welcome-widgets-menus',
		'supports'            => ['title', 'editor', 'revisions'],
	];

	const TAXONOMY = 'customblockpatterntype';
	const TAX_ARGS = [
		'show_in_rest'      => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
		'has_archive'       => false,
		'rewrite'           => [
			'slug'       => 'customblockpattern/category',
			'with_front' => false
		],
		'label'             => 'Block Pattern Categories',
		'labels'            => [
			'name'                  => 'Categories',
			'singular_name'         => 'Block Pattern Category',
			'menu_name'             => 'Block Pattern Categories',
			'search_items'          => 'Search block pattern categories',
			'all_items'             => 'All block pattern categories',
			'parent_item'           => 'Parent block pattern category',
			'parent_item_colon'     => 'Parent block pattern category:',
			'edit_item'             => 'Edit block pattern category',
			'update_item'           => 'Update block pattern category',
			'add_new_item'          => 'Add new block pattern category',
			'new_item_name'         => 'New block pattern category name',
			'not_found'             => 'No block pattern categories found',
			'item_link'             => 'Block Pattern Category Link',
			'item_link_description' => 'A link to a block pattern category.',
		],
	];

	const BLOCK_PATTERN_SCRIPT_HANDLE = 'add-to-custom-block-patterns-js';


	public static function setup()
	{
		/**
		 * if we want to force the posttype to be registered
		 */
		$force_register_posttype = apply_filters('vital_cbp_force_register_posttype', false);
		if ($force_register_posttype) {
			self::register_hooks();
			return;
		}

		/**
		 * let's the theme or other plugins declare whether
		 * the posttype should be registered IF there are
		 * any posts of that posttype
		 */
		$should_register_posttype = apply_filters('vital_cbp_register_posttype', true);
		if (!$should_register_posttype) {
			return;
		}

		//bail if there are no posts
		if (!self::has_any_posts()) {
			return;
		}

		self::register_hooks();
	}

	public static function register_hooks() {
		add_action('init', [__CLASS__, 'register_taxonomy'], 9);
		add_action('init', [__CLASS__, 'register_post_type'], 10);
		add_action('current_screen', [__CLASS__, 'current_screen']);

		add_action('init', [__CLASS__, 'register_block_editor_scripts']);
		add_action('enqueue_block_editor_assets', function () {
			wp_enqueue_script(self::BLOCK_PATTERN_SCRIPT_HANDLE);
		});
		add_action('wp_enqueue_scripts', [__CLASS__, 'enqueue_styles']);
		add_action('enqueue_block_editor_assets', [__CLASS__, 'enqueue_styles']);

		add_action('add_meta_boxes', [__CLASS__, 'add_instructions_metabox']);
		add_action('save_post', [__CLASS__, 'save_metabox_data']);
	}

	public static function register_post_type()
	{
		register_post_type(self::SLUG, self::CPT_ARGS);
	}

	public static function register_taxonomy()
	{
		register_taxonomy(self::TAXONOMY, self::SLUG, self::TAX_ARGS);
	}

	private static function has_any_posts() {
		global $wpdb;
		$rows = $wpdb->get_var($wpdb->prepare("
                    SELECT COUNT(P.id) 
					FROM {$wpdb->posts} AS P 
					WHERE P.post_type = %s
                ", self::SLUG)); 

		if (!is_numeric($rows)) {
			return false;
		}

		$rows = (int) $rows;

		return $rows > 0;
	}

	/**
	 * custom SQL query to pull a list of our
	 * posttype instances
	 *
	 * @return array|object|null
	 */
	private static function get_block_patterns()
	{
		global $wpdb;

		$block_patterns = $wpdb->get_results($wpdb->prepare("
			SELECT P.id, P.post_title, P.post_content, T.slug as category
			FROM {$wpdb->posts} AS P
			LEFT JOIN {$wpdb->term_relationships} AS TR ON TR.object_id = P.id
			LEFT JOIN {$wpdb->terms} AS T ON T.term_id = TR.term_taxonomy_id
			WHERE post_type = %s
			AND post_status = %s
		", self::SLUG, 'publish'));

		return $block_patterns;
	}

	private static function register_block_pattern_categories()
	{
		register_block_pattern_category(self::SLUG, [
			'label' => __('Custom Block Patterns'),
		]);

		$terms = get_terms(['taxonomy' => self::TAXONOMY]);
		foreach ($terms as $term) {
			$name = sprintf('%s/%s', self::SLUG, $term->slug);
			register_block_pattern_category($name, ['label' => $term->name]);
		}
	}

	/**
	 * registers our custom posts as patterns in the editor
	 *
	 * @return void
	 */
	public static function current_screen()
	{
		$screen = get_current_screen();
		if (!$screen->is_block_editor) {
			return;
		}

		/**
		 * only register if we have any remaining
		 * customblockpattern posts
		 */
		$block_patterns = self::get_block_patterns();
		if (empty($block_patterns)) {
			return;
		}

		self::register_block_pattern_categories();

		foreach ($block_patterns as $bp) {
			$cats = [self::SLUG];
			if ($bp->category) {
				$cats[] = sprintf('%s/%s', self::SLUG, $bp->category);
			}

			register_block_pattern(
				sprintf('%s/%s', self::SLUG, sanitize_title($bp->post_title)),
				[
					'title'      => $bp->post_title,
					'categories' => $cats,
					'content'    => $bp->post_content,
				]
			);
		}
	}

	/**
	 * register what we'll need for scripts on the editor
	 *
	 * @return void
	 */
	public static function register_block_editor_scripts()
	{
		$file = Custom_Block_Patterns::instance()->path . '/build/index.js';
		if (!file_exists($file)) {
			return;
		}
		wp_register_script(
			self::BLOCK_PATTERN_SCRIPT_HANDLE,
			Custom_Block_Patterns::instance()->url . '/build/index.js',
			[
				'wp-block-editor',
				'wp-blocks',
				'wp-i18n',
				'wp-plugins',
				'wp-edit-post',
				'wp-element',
				'wp-components',
				'wp-notices',
				'wp-data',
			],
			filemtime($file)
		);
	}

	public static function enqueue_styles()
	{
		$file = Custom_Block_Patterns::instance()->path . '/build/index.css';
		if (!file_exists($file)) {
			return;
		}

		if (!is_post_type_archive('customblockpattern')) {
			return;
		}

		wp_enqueue_style(
			'vital_cbp_styles',
			Custom_Block_Patterns::instance()->url . '/build/index.css',
			[],
			filemtime($file)
		);
	}

	public static function add_instructions_metabox()
	{
		add_meta_box(
			'vital_cbp_instructions_mb',
			__('Additional Block Pattern Settings', 'vital'),
			[__CLASS__, 'instructions_mb_html'],
			self::SLUG
		);
	}

	public static function save_metabox_data($post_id)
	{
		if (array_key_exists('vital-cbp-instructions', $_POST)) {
			update_post_meta($post_id, 'vital-cbp-instructions', wp_kses_post($_POST['vital-cbp-instructions']));
		}
	}

	public static function instructions_mb_html($post)
	{
		$value = get_post_meta($post->ID, 'vital-cbp-instructions', true);
?>
		<label for="vital-cpb-instructions">Instructions/Description</label>
		<p class="description">OPTIONAL: provide instructions on the intended use or execution for this Custom Block Pattern.</p>
<?php
		wp_editor(
			$value,
			'vital-cbp-instructions',
			array(
				'textarea_name' => 'vital-cbp-instructions',
				'media_buttons' => false,
				'tinymce'       => true,
				'teeny'         => false,
				'wpautop'       => true
			)

		);
	}
}

add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Block_Pattern_CPT', 'setup']);
