<?php

namespace Vital\Custom_Block_Patterns;

use Vital\Custom_Block_Patterns;

defined('ABSPATH') || exit;

class Block_Pattern_Archives {
	public static function setup() {
		add_action('init', [__CLASS__, 'rewrite_block_url']);

		add_action('wp_enqueue_scripts', [__CLASS__, 'enqueue_archive_scripts']);
		add_action('parse_query', [__CLASS__, 'remove_paging_from_archive']);
		add_action('template_redirect', [__CLASS__, 'maybe_redirect_single_post']);
		add_action('archive_template', [__CLASS__, 'maybe_override_archive_template']);
		add_action('pre_get_posts', [__CLASS__, 'register_archive_actions']);
		add_filter('register_post_type_args', [__CLASS__, 'maybe_adjust_post_type_archive_publicity'], 9, 2);
		add_action('wp_head', [__CLASS__, 'maybe_add_meta_tag'], 1);

		add_action('pre_get_posts', [__CLASS__, 'add_posttype_to_term'], 9);
		add_action('pre_get_posts', [__CLASS__, 'only_unsynced_blocks']);
	}

	public static function rewrite_block_url() {
		$listen = '^' . Custom_Block_Patterns::SLUG;
		$redirect = 'index.php?post_type=' . Custom_Block_Patterns::SLUG;

		add_rewrite_rule(
			$listen,
			$redirect,
			'top'
		);
	}

	public static function add_posttype_to_term($query) {
		if (is_admin()) {
			return;
		}

		if (!$query->is_main_query()) {
			return;
		}

		// we only need to add the posttype to the term page
		if (!is_tax(Custom_Block_Patterns::TAXONOMY)) {
			return;
		}

		$post_types = $query->get('post_type');

		if (!is_array($post_types)) {
			if (empty($post_types)) {
				$post_types = [];
			} else {
				$post_types = explode(',', $post_types);
			}
		}

		$post_types = array_filter($post_types); //remove any empty items

		if (!$post_types) {
			$post_types = [Custom_Block_Patterns::SLUG];
		} elseif (!in_array(Custom_Block_Patterns::SLUG, $post_types, true)) {
			$post_types[] = Custom_Block_Patterns::SLUG;
		}

		$query->set('post_type', $post_types);
	}

	public static function only_unsynced_blocks($query) {
		if (is_admin()) {
			return;
		}

		if (!$query->is_main_query()) {
			return;
		}

		// we only need to add the posttype to the term page
		if (is_post_type_archive(Custom_Block_Patterns::SLUG) || is_tax(Custom_Block_Patterns::TAXONOMY)) {
			$meta_query = [
				[
					'key'     => 'wp_pattern_sync_status',
					'value'   => [],
					'compare' => '==',
				],
			];

			$query->set('meta_query', $meta_query);

			$tax_query = [
				[
					'taxonomy' => 'wp_pattern_type',
					'field'    => 'slug',
					'terms'    => ['hide-from-grid'],
					'operator' => 'NOT IN',
				],
			];

			$query->set('tax_query', $tax_query);
		}
	}

	/**
	 * Enqueue any extra assets needed on the Pattern archive
	 */
	public static function enqueue_archive_scripts() {
		if (!is_post_type_archive(Custom_Block_Patterns::SLUG)) {
			return;
		}

		$click_to_copy_asset = include __DIR__ . '/../build/click-to-copy-url.asset.php';
		if (!$click_to_copy_asset) {
			return;
		}

		$url = plugins_url('vital-custom-block-patterns/build/click-to-copy-url.js');

		wp_enqueue_script(
			'vtl_click_to_copy_text',
			$url,
			$click_to_copy_asset['dependencies'],
			$click_to_copy_asset['version'],
			true
		);

		wp_enqueue_style(
			'vital_custom_block_patterns_archive',
			plugins_url('vital-custom-block-patterns/build/index.css'),
			[],
			true
		);
	}

	public static function register_archive_actions($query) {
		if (\is_admin()) {
			return;
		}

		if (!$query->is_main_query()) {
			return;
		}

		if (!is_archive()) {
			return;
		}

		$post_types = $query->get('post_type');
		if (!is_array($post_types)) {
			$post_types = [$post_types];
		}

		if (!in_array(Custom_Block_Patterns::SLUG, $post_types, true)) {
			return;
		}

		add_action('before_vital_cbp_archive_loop', [__CLASS__, 'inject_archive_heading']);
		add_action('before_vital_cbp_archive_loop', [__CLASS__, 'inject_archive_meta']);

		add_action('before_vital_cbp_archive_post', [__CLASS__, 'inject_post_anchor']);
		add_action('before_vital_cbp_archive_post', [__CLASS__, 'inject_post_title']);
		add_action('before_vital_cbp_archive_post', [__CLASS__, 'inject_post_terms']);

		add_action('the_vital_cbp_archive_post', [__CLASS__, 'the_archive_post']);

		add_action('after_vital_cbp_archive_post', [__CLASS__, 'maybe_inject_description']);
		add_action('after_vital_cbp_archive_post', [__CLASS__, 'inject_post_meta']);
		add_action('after_vital_cbp_archive_post', [__CLASS__, 'inject_delimiter']);
	}

	public static function taxonomy_template($template) {
		if (!is_tax(Custom_Block_Patterns::TAXONOMY)) {
			return $template;
		}

		if ($local_template = self::get_local_template()) {
			return $local_template;
		}

		return $template;
	}

	/**
	 * adds a noindex,nofollow meta tag
	 * for the block pattern archive and block pattern category taxonomy terms
	 *
	 * @return void
	 */
	public static function maybe_add_meta_tag() {
		if (is_post_type_archive(Custom_Block_Patterns::SLUG) || is_tax(Custom_Block_Patterns::TAXONOMY)) {
?>
			<meta name="robots" content="noindex,nofollow">
		<?php
		}
	}

	/**
	 * injects a heading to the archive
	 *
	 * @return void
	 */
	public static function inject_archive_heading() {
		$heading = __('Custom Block Patterns', 'vital');
		if (is_tax(Custom_Block_Patterns::TAXONOMY)) {
			$heading .= ' : ' . single_term_title('', false);
		} ?>
		<h1><?php echo $heading; ?></h1>
	<?php
	}

	public static function inject_archive_meta($qo) {
		global $wp_query; ?>
		<section class="vital-cbp-archive-meta-container">
			<div class="vital-cbp-archive-result-count">
				<?php
				printf(
					'Viewing %s pattern%s',
					number_format($wp_query->post_count),
					$wp_query->post_count === 1 ? '' : 's'
				); ?>
			</div>
			<div class="vital-cbp-archive-filters">
				<?php
				//show a select element with the terms as arguments
				$terms = get_terms([
					'taxonomy'   => Custom_Block_Patterns::TAXONOMY,
					'hide_empty' => true,
				]);
				if ($terms && !is_wp_error($terms)) {
					$field_id = Custom_Block_Patterns::TAXONOMY . '__taxonomy_filter';
					if (is_a($qo, 'WP_Term') && $qo->taxonomy === Custom_Block_Patterns::TAXONOMY) {
						$selected_term_id = (int)$qo->term_id;
					} else {
						$selected_term_id = null;
					} ?>
					<label for="<?php echo $field_id; ?>">Show:</label>
					<select name="taxonomy-filter" id="<?php echo $field_id; ?>" onchange="window.location.href=this.value;return;">
						<option value="<?php echo get_post_type_archive_link(Custom_Block_Patterns::SLUG); ?>">All Categories</option>
						<?php foreach ($terms as $term) { ?>
							<option value="<?php echo get_term_link($term); ?>" <?php echo $selected_term_id === $term->term_id ? ' selected' : ''; ?>><?php echo $term->name; ?></option>
						<?php } ?>
					</select>
				<?php
				} ?>
			</div>
		</section>
	<?php
	}

	/**
	 * inject an anchor element for the post
	 *
	 * @param WP_Post $post
	 * @return void
	 */
	public static function inject_post_anchor($post) {
	?>
		<a name="vital-cbp-anchor-<?php echo $post->ID; ?>"></a>
	<?php
	}

	/**
	 * inject the title of the post
	 *
	 * @param WP_Post $post
	 * @return void
	 */
	public static function inject_post_title($post) {
		global $wp;

		$anchor_url = sprintf(
			'%s#vital-cbp-anchor-%s',
			home_url($wp->request),
			$post->ID
		);

		// the name of the post
		printf(
			'<h2 id="vital-cbp-anchor-%s" class="vital-cbp-title">%s %s %s</h2>',
			$post->ID,
			get_the_title($post),
			self::_get_copy_url_button($anchor_url),
			self::_get_copy_pattern_button($post)
		);
	}

	private static function _style_array_to_string($arr) {
		if (empty($arr) || !is_array($arr)) {
			return '';
		}

		$keys = array_keys($arr);
		return array_reduce($keys, function ($output, $key) use ($arr) {
			return $output . sprintf('%s:%s;', $key, $arr[$key]);
		}, '');
	}

	private static function _get_copy_pattern_button($pattern) {
		return sprintf(
			'<button class="copy-button copy-pattern-button" data-click-to-copy="%s">%s</button>',
			esc_attr($pattern->post_content),
			__('Copy Pattern', 'vitaldesign')
		);
	}

	private static function _get_copy_url_button($text, $label = 'Copy Link') {
		$copy_link_button = sprintf(
			'<button class="copy-button copy-link-button" data-click-to-copy="%s">%s</button>',
			$text,
			$label
		);

		return $copy_link_button;
	}

	/**
	 * injects a list of the terms associated with the post
	 *
	 * @param WP_Post $post
	 * @return void
	 */
	public static function inject_post_terms($post) {
		// add in any taxonomy terms
		$terms = get_the_terms($post, Custom_Block_Patterns::TAXONOMY);
		if (!$terms || is_wp_error($terms)) {
			return;
		} ?>
		<section class="vital-cbp-pattern-terms">
			<?php foreach ($terms as $term) { ?>
				<div class="vital-cbp-pattern-term vital-cbp-pattern-term-<?php $term->id; ?>"><?php echo $term->name; ?></div>
			<?php } ?>
		</section>
	<?php
	}

	public static function the_archive_post($post) {
		echo apply_filters('the_content', $post->post_content);
	}

	/**
	 * returns an edit link DOM element if the user
	 * can edit this post, otherwise returns null
	 *
	 * @param WP_Post $post
	 * @return null|string
	 */
	private static function _get_edit_link($post) {
		// since get_edit_post_link won't add the edit vars
		// and instead just routes the user to the archive
		// we'll bail if the current user can't edit this post
		if (!\current_user_can('edit_post', $post->ID)) {
			return;
		}

		$pt_object = \get_post_type_object(Posttype_Customizations::SLUG);
		return sprintf(
			'<a href="%s">%s</a>',
			\get_edit_post_link($post),
			\__(sprintf('Edit this %s', strtolower($pt_object->labels->singular_name)), 'vital'),
		);
	}

	public static function inject_post_meta($post) {
		$updated_date = \get_the_modified_time('F jS, Y', $post);
		$updated_time = \get_the_modified_time('g:i a T', $post);

		$meta_items = [];
		if ($edit_link = self::_get_edit_link($post)) {
			$meta_items[] = $edit_link;
		}
		$meta_items[] = sprintf(
			'<span class="vital-cbp-last-updated-date">%s %s %s %s</span>',
			\__('Last updated on', 'vital'),
			$updated_date,
			\__('at', 'vital'),
			$updated_time
		);

		if (!$meta_items) {
			return;
		}

		printf('<div class="vital-cbp-pattern-meta">%s</div>', implode(' | ', $meta_items));
	}

	public static function inject_delimiter() {
		echo render_block([
			'blockName'    => 'core/separator',
			'attrs'        => [
				'class' => 'vital-cpb-post-divider',
			],
			'innerHTML'    => '<hr class="wp-block-separator has-alpha-channel-opacity vital-cpb-post-divider">',
			'innerContent' => [
				'<hr class="wp-block-separator has-alpha-channel-opacity vital-cpb-post-divider">',
			],
			'innerBlocks'  => [],
		]);
	}

	public static function maybe_inject_description($post) {
		$value = get_post_meta($post->ID, 'vital-cbp-instructions', true);
		if (!$value) {
			return;
		}

		$pt_object = \get_post_type_object(Posttype_Customizations::SLUG);
	?>
		<section class="vital-cbp-description">
			<h5 class="vital-cbp-description-heading"><?php echo $pt_object->labels->singular_name; ?> Footnotes:</h5>
			<?php echo wpautop(wp_kses_post($value)); ?>
		</section>
<?php
	}

	/**
	 * when viewing the archive for this posttype,
	 * if the options are NOT set to use the theme's archive template
	 * we'll use the template included with the plugin
	 *
	 * @param string $template
	 * @return string
	 */
	public static function maybe_override_archive_template($template) {
		if (!is_post_type_archive(Custom_Block_Patterns::SLUG) && !is_tax(Custom_Block_Patterns::TAXONOMY)) {
			return $template;
		}

		if ($local_template = self::get_local_template()) {
			return $local_template;
		}

		return $template;
	}

	/**
	 * helper function to retrieve and verify the
	 * local template
	 *
	 * @return string|null
	 */
	public static function get_local_template() {
		//$options = get_option(Block_Pattern_Options::OPTIONS_SLUG);
		//$use_theme_template = $options['vital_cbp_use_theme_archive_template'] ?? false;

		//if ($use_theme_template) {
		//    return;
		//}

		$local_template = Custom_Block_Patterns::instance()->path . 'templates/archive.php';
		// if our local template exists, use it!
		if (file_exists($local_template)) {
			return $local_template;
		}

		error_log($local_template . ' does not exist!');
	}

	/**
	 * allows for setting public availability to the archive
	 * if the local settings in this class are not set to public
	 * then the options setting will be used for determining public availability
	 *
	 * @param array $args
	 * @param string $post_type
	 * @return array
	 */
	public static function maybe_adjust_post_type_archive_publicity($args, $post_type) {
		if ($post_type !== Custom_Block_Patterns::SLUG) {
			return $args;
		}

		/**
		 * if the post_type definition is already set to public
		 * then we'll just take that
		 */
		$public = $args['public'] ?? false;
		if ($public) {
			return $args;
		}

		//$options = get_option(Block_Pattern_Options::OPTIONS_SLUG);
		//$archive_public = $options['vital_cbp_archive_visibility'] ?? false;
		//if ($archive_public) {
		$args['public'] = true;
		$args['has_archive'] = true;
		//}

		//check to see if options are set to public
		return $args;
	}

	// redirect single posts to the archive page, scrolled to current ID
	public static function maybe_redirect_single_post() {
		if (is_singular(Custom_Block_Patterns::SLUG)) {
			global $post;
			$redirectLink = sprintf(
				'%s#vital-cbp-anchor-%d',
				get_post_type_archive_link(Custom_Block_Patterns::SLUG),
				$post->ID
			);
			wp_redirect($redirectLink, 302);
			exit;
		}
	}


	/**
	 * shuts off the paging of the archive posts
	 *
	 * uses the filter `vital_cbp_no_paging` to determine
	 * if paging should be shut off. The default is 'true'.
	 *
	 * @param WP_Query $query
	 * @return void
	 */
	public static function remove_paging_from_archive($query) {
		if (is_admin()) {
			return;
		}

		if (!is_post_type_archive(Custom_Block_Patterns::SLUG) && !is_tax(Custom_Block_Patterns::TAXONOMY)) {
			return;
		}
		if (!$query->is_main_query()) {
			return;
		}
		if (apply_filters('vital_cbp_no_paging', true)) {
			$query->set('nopaging', 1);
		}
	}
}

add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Block_Pattern_Archives', 'setup']);
