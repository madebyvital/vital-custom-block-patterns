<?php

namespace Vital\Custom_Block_Patterns;

use Vital\Custom_Block_Patterns;
use Vital\Custom_Block_Patterns\Taxonomies\Pattern_Type;

defined('ABSPATH') || exit;

class Posttype_Customizations
{
	const SLUG = 'wp_block';
    const PATTERN_SVG = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBmaWxsPSJibGFjayIgZD0iTTIxLjMgMTAuOGwtNS42LTUuNmMtLjctLjctMS44LS43LTIuNSAwbC01LjYgNS42Yy0uNy43LS43IDEuOCAwIDIuNWw1LjYgNS42Yy4zLjMuOC41IDEuMi41cy45LS4yIDEuMi0uNWw1LjYtNS42Yy44LS43LjgtMS45LjEtMi41em0tMSAxLjRsLTUuNiA1LjZjLS4xLjEtLjMuMS0uNCAwbC01LjYtNS42Yy0uMS0uMS0uMS0uMyAwLS40bDUuNi01LjZzLjEtLjEuMi0uMS4xIDAgLjIuMWw1LjYgNS42Yy4xLjEuMS4zIDAgLjR6bS0xNi42LS40TDEwIDUuNWwtMS0xLTYuMyA2LjNjLS43LjctLjcgMS44IDAgMi41TDkgMTkuNWwxLjEtMS4xLTYuMy02LjNjLS4yIDAtLjItLjItLjEtLjN6Ij48L3BhdGg+PC9zdmc+Cg==';

	public static function setup()
	{
		\add_action('add_meta_boxes', [__CLASS__, 'add_instructions_metabox']);
		\add_action('save_post', [__CLASS__, 'save_metabox_data']);
        \add_action('admin_menu', [__CLASS__, 'customize_admin_sidebar']);
        \add_action('admin_menu', [__CLASS__, 'add_pattern_category_to_sidebar']);
        \add_action('admin_menu', [__CLASS__, 'add_pattern_type_to_sidebar']);
		\add_filter('post_link', [__CLASS__, 'maybe_alter_post_type_link'], PHP_INT_MAX, 2);
		\add_filter('searchwp_related_excluded_post_types', [__CLASS__, 'add_to_searchwp_related_posttypes'], PHP_INT_MAX, 2);
		\add_filter('duplicate_post_enabled_post_types', [__CLASS__, 'add_to_duplicate_post_posttypes'], PHP_INT_MAX);
	}

	public static function modify_wp_block_archive($args, $post_type) {
		if ($post_type !== self::SLUG) {
			return $args;
		}

		$args = array_merge($args, [
			'public'              => false, // fixes yoast bug https://vital.teamwork.com/app/tasks/31919035
			'publicly_queryable'  => true,
			'show_in_menu'        => true,
			//'has_archive'         => 'patterns',
			'exclude_from_search' => true,
			'show_in_nav_menus'   => true,
			/*
			'rewrite'             => [
				'slug'       => 'pattern',
				'with_front' => true,
			],
			*/
		]);

		return $args;
	}

	// allows block patterns to be duplicated
	public static function add_to_duplicate_post_posttypes($post_types) {
		if (!in_array(self::SLUG, $post_types, true)) {
			$post_types[] = self::SLUG;
		}
		return $post_types;
	}
	// removes from searchwp related interface
	public static function add_to_searchwp_related_posttypes($post_types) {
		if (!in_array(self::SLUG, $post_types, true)) {
			$post_types[] = self::SLUG;
		}

		return $post_types;
	}
	public static function get_singular_link(\WP_Post $post) {
		return sprintf(
			'%s#vital-cbp-anchor-%d',
			get_post_type_archive_link(self::SLUG),
			$post->ID
		);
	}

	public static function maybe_alter_post_type_link($link, $post) {
		if (get_post_type($post) !== self::SLUG) {
			return $link;
		}

		return self::get_singular_link($post);
	}

	/**
	 * Changes core "WP Block" post type labels.
	 *
	 * @access public
	 * @since  1.0.0
	 * @return void
	 */
	public static function modify_wp_block_labels($args, $posttype_slug) {
		if ($posttype_slug !== self::SLUG) {
			return $args;
		}

		$args['labels'] = array_merge($args['labels'], [
			'name' => __('Block Patterns'),
            'singular_name' => __('Block Pattern'),
            'add_new' => __('Add New Block Pattern'),
            'add_new_item' => __('Add New Block Pattern'),
            'new_item' => __('New Block Pattern'),
            'edit_item' => __('Edit Block Pattern'),
            'view_item' => __('View Block Pattern'),
            'view_items' => __('View Block Patterns'),
            'all_items' => __('All Block Patterns'),
            'search_items' => __('Search Block Patterns'),
            'not_found' => __('No block patterns found.'),
            'not_found_in_trash' => __('No block patterns found in Trash.'),
            'filter_items_list' => __('Filter block patterns list'),
            'items_list_navigation' => __('Block Patterns list navigation'),
            'items_list' => __('Block Patterns list'),
            'item_published' => __('Block Pattern published.'),
            'item_published_privately' => __('Block Pattern published privately.'),
            'item_reverted_to_draft' => __('Block Pattern reverted to draft.'),
            'item_scheduled' => __('Block Pattern scheduled.'),
            'item_updated' => __('Block Pattern updated.'),
		]);

		return $args;
	}


	public static function add_instructions_metabox()
	{
		$pt_object = \get_post_type_object(self::SLUG);
		\add_meta_box(
			'vital_cbp_instructions_mb',
			\__(sprintf('Additional %s Settings', $pt_object->labels->singular_name), 'vital'),
			[__CLASS__, 'instructions_mb_html'],
			self::SLUG
		);
	}

	public static function save_metabox_data($post_id)
	{
		if (array_key_exists('vital-cbp-instructions', $_POST)) {
			\update_post_meta($post_id, 'vital-cbp-instructions', \wp_kses_post($_POST['vital-cbp-instructions']));
		}
	}

	public static function instructions_mb_html($post)
	{
		$value = \get_post_meta($post->ID, 'vital-cbp-instructions', true);
		$pt_object = \get_post_type_object(self::SLUG);
		?>
		<label for="vital-cpb-instructions">Instructions/Description</label>
		<p class="description">OPTIONAL: provide instructions on the intended use or execution for this <?php echo $pt_object->labels->singular_name; ?>.</p>
		<?php
		\wp_editor(
			$value,
			'vital-cbp-instructions',
			array(
				'textarea_name' => 'vital-cbp-instructions',
				'media_buttons' => false,
				'tinymce'       => true,
				'teeny'         => false,
				'wpautop'       => true
			)

		);
	}


    public static function customize_admin_sidebar() {
		$pt_object = \get_post_type_object(self::SLUG);
        \add_menu_page('linked_url', $pt_object->labels->name, 'read', 'edit.php?post_type=wp_block', '', self::PATTERN_SVG);
    }
    public static function add_pattern_category_to_sidebar() {
		$taxonomy_object = \get_taxonomy(Custom_Block_Patterns::TAXONOMY);
		if (!$taxonomy_object) {
			return;
		}
		\add_submenu_page(
			sprintf('edit.php?post_type=%s', self::SLUG),
			$taxonomy_object->labels->name,
			$taxonomy_object->labels->name,
			'read',
			sprintf('edit-tags.php?post_type=%s&taxonomy=%s', self::SLUG, Custom_Block_Patterns::TAXONOMY),
		);
    }
    public static function add_pattern_type_to_sidebar() {
		$taxonomy_object = \get_taxonomy(Pattern_Type::SLUG);
		if (!$taxonomy_object) {
			return;
		}

		\add_submenu_page(
			sprintf('edit.php?post_type=%s', self::SLUG),
			$taxonomy_object->labels->name,
			$taxonomy_object->labels->name,
			'read',
			sprintf('edit-tags.php?post_type=%s&taxonomy=%s', self::SLUG, Pattern_Type::SLUG),
		);
    }
}

\add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Posttype_Customizations', 'setup']);
\add_action('register_post_type_args', ['\\Vital\\Custom_Block_Patterns\\Posttype_Customizations', 'modify_wp_block_labels'], PHP_INT_MAX, 2);
\add_action('register_post_type_args', ['\\Vital\\Custom_Block_Patterns\\Posttype_Customizations', 'modify_wp_block_archive'], PHP_INT_MAX, 2);
