<?php

namespace Vital\Custom_Block_Patterns\Taxonomies;

defined('ABSPATH') || exit;

class Pattern_Category_Customizations
{
	const SLUG = 'wp_pattern_category';

	public static function modify_taxonomy_args($args, $taxonomy_slug) {
		if ($taxonomy_slug !== self::SLUG) {
			return $args;
		}

		$singular = 'Pattern Category';
		$plural = 'Pattern Categories';
		$args['labels'] = array_merge($args['labels'], [
			'name'                       => _x($plural, 'taxonomy general name'),
			'singular_name'              => _x($singular, 'taxonomy singular name'),
			'search_items'               => __(sprintf('Search %s', $plural)),
			'popular_items'              => __(sprintf('Popular %s', $plural)),
			'all_items'                  => __(sprintf('All %s', $plural)),
			'parent_item'                => __(sprintf('Parent %s', $singular)),
			'parent_item_colon'          => __(sprintf('Parent %s:', $singular)),
			'edit_item'                  => __(sprintf('Edit %s', $singular)),
			'view_item'                  => __(sprintf('View %s', $singular)),
			'update_item'                => __(sprintf('Update %s', $singular)),
			'add_new_item'               => __(sprintf('Add New %s', $singular)),
			'new_item_name'              => __(sprintf('New %s', $singular)),
			'separate_items_with_commas' => __(sprintf('Separate %s with commas', strtolower($plural))),
			'add_or_remove_items'        => __(sprintf('Add or remove %s', strtolower($plural))),
			'choose_from_most_used'      => __(sprintf('Choose from most used %s', strtolower($plural))),
			'not_found'                  => __(sprintf('No %s found', strtolower($plural))),
			'no_terms'                   => __(sprintf('No %s', $plural)),
			'filter_by_item'             => __(sprintf('Filter by %s', strtolower($plural))),
			'back_to_items'              => __(sprintf('Back to %s', $plural)),
		]);

		return $args;
	}
}
add_action('register_taxonomy_args', ['\\Vital\\Custom_Block_Patterns\\Taxonomies\\Pattern_Category_Customizations', 'modify_taxonomy_args'], PHP_INT_MAX, 2);
