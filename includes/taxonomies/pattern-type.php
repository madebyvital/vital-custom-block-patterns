<?php

namespace Vital\Custom_Block_Patterns\Taxonomies;

defined('ABSPATH') || exit;

class Pattern_Type {
	const SLUG = 'wp_pattern_type';
	public static function setup() {
		add_action('init', [__CLASS__, 'create_block_type_taxonomy'], 5);
		add_action('init', [__CLASS__, 'add_archive_visibility_term'], 10);
	}

	public static function create_block_type_taxonomy() {
		register_taxonomy(
			self::SLUG,
			['wp_block'],
			[
				'public'             => true,
				'publicly_queryable' => true,
				'hierarchical'       => true,
				'labels'             => [
					'name'                       => _x('Pattern Types', 'taxonomy general name'),
					'singular_name'              => _x('Pattern Type', 'taxonomy singular name'),
					'search_items'               => __('Search Pattern Types'),
					'popular_items'              => __('Popular Pattern Types'),
					'all_items'                  => __('All Pattern Types'),
					'parent_item'                => __('Parent Pattern Type'),
					'parent_item_colon'          => __('Parent Pattern Type:'),
					'edit_item'                  => __('Edit Pattern Type'),
					'view_item'                  => __('View Pattern Type'),
					'update_item'                => __('Update Pattern Type'),
					'add_new_item'               => __('Add New Pattern Type'),
					'new_item_name'              => __('New Pattern Type'),
					'separate_items_with_commas' => __('Separate pattern types with commas'),
					'add_or_remove_items'        => __('Add or remove pattern types'),
					'choose_from_most_used'      => __('Choose from most used pattern types'),
					'not_found'                  => __('No pattern types found'),
					'no_terms'                   => __('No Pattern Types'),
					'filter_by_item'             => __('Filter by pattern type'),
					'back_to_items'              => __('Back to Pattern Types'),
				],
				'query_var'          => true,
				'rewrite'            => ['slug' => 'wp-pattern-type'],
				'show_ui'            => true,
				'_builtin'           => false,
				'show_in_nav_menus'  => true,
				'show_in_rest'       => true,
				'show_admin_column'  => true,
			]
		);
	}

	public static function add_archive_visibility_term() {
		if (!term_exists('hide-from-grid', self::SLUG)) {
			wp_insert_term(
				'Hidden Pattern',
				self::SLUG,
				[
					'description' => 'Add this term to hide this pattern from the <a href="/wp_block" target="_blank">pattern archive grid</a>.',
					'slug'        => 'hide-from-grid',
				]
			);
		}
	}
}

add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Taxonomies\\Pattern_Type', 'setup']);
