<?php
namespace Vital\Custom_Block_Patterns;

defined('ABSPATH') || exit;

class Block_Pattern_Options {
	const MENU_SLUG = 'customblockpatterns_options';
	const OPTIONS_SLUG = 'vital_cbp_options';

	public static function setup() {
		add_action('admin_menu', [__CLASS__, 'add_options_page']);
		add_action('admin_init', [__CLASS__, 'options_page_initialization']);
		add_action('update_option_vital_cbp_options', [__CLASS__, 'after_options_save'], 10, 2);
	}

	/**
	 * when viewing the archive for this posttype,
	 * if the options are NOT set to use the theme's archive template
	 * we'll use the template included with the plugin
	 *
	 * @param string $template
	 * @return string
	 */
	public static function maybe_override_archive_template($template) {
		if (!is_post_type_archive(Block_Pattern_CPT::SLUG)) {
			return $template;
		}

		$options = get_option( 'vital_cbp_options' );
		$use_theme_template = $options['vital_cbp_use_theme_archive_template']?? false;
		if ($use_theme_template) {
			return $template;
		}

		$local_template = plugin_dir_path(__FILE__) . 'templates/archive.php';
		// if our local template exists, use it!
		if (file_exists($local_template)) {
			return $local_template;
		}

		error_log($local_template . ' does not exist! Reverting to use default archive template.');

		return $template;
	}

	/**
	 * add an options page for this posttype
	 *
	 * @return void
	 */
	public static function add_options_page() {
		add_submenu_page(
			sprintf('edit.php?post_type=%s', Block_Pattern_CPT::SLUG),
			__('Block Patterns Options'),
			__('Options'),
			'manage_options',
			SELF::MENU_SLUG,
			[__CLASS__, 'options_page']
		);
	}

	/**
	 * if the visibility of the archive has changed, flush the rewrite rules
	 *
	 * @param mixed $old_value
	 * @param mixed $new_value
	 * @return void
	 */
	public static function after_options_save($old_value, $new_value) {
		$old_visibility = $old_value['vital_cbp_archive_visibility']?? false;
		$new_visibility = $new_value['vital_cbp_archive_visibility']?? false;
		if ($old_visibility !== $new_visibility) {
			flush_rewrite_rules();
		}
	}
	/**
	 * register the settings for the options page
	 *
	 * @return void
	 */
	public static function options_page_initialization() {
		// add our options container
		register_setting(
			self::MENU_SLUG,
			'vital_cbp_options',
		);

		// add a section to the options container
		add_settings_section(
			'vital_cbp_visibility_options',
			__('Visibility', 'vital'),
			[__CLASS__, 'visibility_options_section_callback'],
			self::MENU_SLUG
		);

		// add a field to the section
		add_settings_field(
			'vital_cbp_archive_visibility',
			__('Make archive visible?', 'vital'),
			[__CLASS__, 'archive_visibility_field_callback'],
			self::MENU_SLUG,
			'vital_cbp_visibility_options',
			array(
				'label_for' => 'vital_cbp_archive_visibility',
			)
		);

		// add a field to the section
		add_settings_field(
			'vital_cbp_use_theme_archive_template',
			__('Use archive template from theme?', 'vital'),
			[__CLASS__, 'archive_template_field_callback'],
			self::MENU_SLUG,
			'vital_cbp_visibility_options',
			array(
				'label_for' => 'vital_cbp_use_theme_archive_template',
			)
		);
	}

	/**
	 * HTML output for the visibility options section
	 *
	 * @return void
	 */
	public static function visibility_options_section_callback() {
		?>
		<p><?php esc_html_e('The Custom Block Patterns are not intended for public consumption. To that end we still will occaisionally need to view the patterns during page-building and other population related instances.', 'vital'); ?></p>
		<?php
	}

	/**
	 * output the HTML for the archive visibility field
	 *
	 * @param array $args
	 * @return void
	 */
	public static function archive_visibility_field_callback($args) {
		// Get the value of the setting we've registered with register_setting()
		$options = get_option(self::OPTIONS_SLUG);
		?>
		<input
			type="checkbox"
			id="<?php echo esc_attr($args['label_for']); ?>"
			name="vital_cbp_options[<?php echo esc_attr($args['label_for']); ?>]"
			value="1"
			<?php echo isset($options[$args['label_for']]) ? checked( $options[$args['label_for']], 1, false ) : ''; ?>
		/>
		<?php
	}

	/**
	 * output the HTML for the archive template field
	 *
	 * @param array $args
	 * @return void
	 */
	public static function archive_template_field_callback($args) {
		// Get the value of the setting we've registered with register_setting()
		$options = get_option(self::OPTIONS_SLUG);
		?>
		<input
			type="checkbox"
			id="<?php echo esc_attr($args['label_for']); ?>"
			name="vital_cbp_options[<?php echo esc_attr($args['label_for']); ?>]"
			value="1"
			<?php echo isset($options[$args['label_for']]) ? checked( $options[$args['label_for']], 1, false ) : ''; ?>
		/>
		<?php
	}

	/**
	 * populate our options page
	 *
	 * @return void
	 */
	public static function options_page() {
		// check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		// add error/update messages

		// check if the user have submitted the settings
		// WordPress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error(
				'vital_cbp_options_messages',
				'vital_cbp_options_message',
				__( 'Settings Saved', 'vital' ),
				'updated'
			);
		}

		// show error/update messages
		settings_errors( 'vital_cbp_options_messages' );
		?>

		<div class="wrap">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<form action="options.php" method="post">
				<?php
				// output security fields for the registered setting "wporg"
				settings_fields( self::MENU_SLUG );
				// output setting sections and their fields
				// (sections are registered for "wporg", each field is registered to a specific section)
				do_settings_sections( self::MENU_SLUG );
				// output save settings button
				submit_button( 'Save Settings' );
				?>
			</form>
		</div>

		<?php
	}
}

add_action('after_setup_theme', ['\\Vital\\Custom_Block_Patterns\\Block_Pattern_Options', 'setup']);
