/**
 * WordPress dependencies
 */
const { createReduxStore, register } = wp.data;

 /**
  * Internal dependencies
  */
import * as actions from './actions';
 
const STORE_NAME = 'vital/custom-block-patterns';

const selectors = {};
 
 /**
  * Store definition for the reusable blocks namespace.
  *
  * @see https://github.com/WordPress/gutenberg/blob/HEAD/packages/data/README.md#createReduxStore
  *
  * @type {Object}
  */
export const store = createReduxStore( STORE_NAME, {
	actions,
	reducer( state ) {
		return state;
	},
	selectors,
	__experimentalUseThunks: true
} );

register( store );