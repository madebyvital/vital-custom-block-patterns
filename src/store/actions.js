/**
 * WordPress dependencies
 */
const { store: blockEditorStore } = wp.blockEditor;
const { serialize } = wp.blocks;
const { __ } = wp.i18n;
 
/**
 * Returns a generator converting one or more static blocks into a reusable block.
 *
 * @param {string[]} clientIds The client IDs of the block to detach.
 * @param {string}   title     Reusable block title.
 */
export const convertBlocksToCustomBlocksPattern = (
	clientIds,
	title
) => async ( { registry } ) => {
	console.log('preparing to dispatch');
	console.log(clientIds);
	console.log(title);
	console.log(registry.select( blockEditorStore ).getBlocksByClientId( clientIds ));

	const blockPattern = {
		title: title || __( 'Untitled Reusable block' ),
		content: serialize(
			registry.select( blockEditorStore ).getBlocksByClientId( clientIds )
		),
		status: 'publish',
	};
	console.log(blockPattern);

	return await registry
		.dispatch( 'core' )
		.saveEntityRecord( 'postType', 'customblockpattern', blockPattern );
};

