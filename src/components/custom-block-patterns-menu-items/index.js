/**
 * WordPress dependencies
 */
 const { withSelect } = wp.data;
 const { store: blockEditorStore } = wp.blockEditor;
 
 /**
  * Internal dependencies
  */
 import CustomBlockPatternsConvertButton from './custom-block-patterns-convert-button';
 
 function CustomBlockPatternsMenuItems( { clientIds, rootClientId } ) {
	 return (
		 <>
			 <CustomBlockPatternsConvertButton
				 clientIds={ clientIds }
				 rootClientId={ rootClientId }
			 />
		 </>
	 );
 }
 
 export default withSelect( ( select ) => {
	 const { getSelectedBlockClientIds } = select( blockEditorStore );
	 return {
		 clientIds: getSelectedBlockClientIds(),
	 };
 } )( CustomBlockPatternsMenuItems );