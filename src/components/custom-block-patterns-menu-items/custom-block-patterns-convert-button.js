const { PluginBlockSettingsMenuItem } = wp.editPost;
const { useCallback, useState } = wp.element;
const { __ } = wp.i18n;
const {
	Modal,
	Button,
	TextControl,
	Flex,
	FlexItem
} = wp.components;
const { store: noticesStore } = wp.notices;
const { useDispatch } = wp.data;
import { store } from '../../store';

function addToCustomBlockPatternsButton( {
	clientIds
} ) {
	const [ isModalOpen, setIsModalOpen ] = useState( false );
	const [ title, setTitle ] = useState( '' );
	const resetModal = () => {
		setIsModalOpen( false );
		setTitle( '' );
	}

	const { convertBlocksToCustomBlocksPattern } = useDispatch( store );
	const { createSuccessNotice, createErrorNotice } = useDispatch(
		noticesStore
	);
	const onConvert = useCallback(
		async function ( patternTitle ) {
			try {
				await convertBlocksToCustomBlocksPattern( clientIds, patternTitle );
				createSuccessNotice( __( 'Block Pattern created.' ), { type: 'snackbar'} );
			} catch ( error ) {
				console.error(error);
				createErrorNotice( error.message );
			}
		},
		[ clientIds ]
	);

	return (
		<>
			<PluginBlockSettingsMenuItem
				icon="welcome-widgets-menus"
				label={__('Add to Block Patterns') }
				onClick={ () => {
					setIsModalOpen( true );
				} }
			/>
			{ isModalOpen && (
				<Modal
					title={__('Create Block Pattern')}
					closeLabel={__('Close')}
					onRequestClose={ resetModal }
				>
					<form
						onSubmit={ (event) => {
							event.preventDefault();
							onConvert( title );
							resetModal();
						}}
					>
						<TextControl
							label={ __( 'Name' ) }
							value={ title }
							onChange={ setTitle }
						></TextControl>
						<Flex
							className="reusable-blocks-menu-items__convert-modal-actions"
							justify="flex-end"
						>
							<FlexItem>
								<Button
									variant="secondary"
									onClick={ resetModal }
								>
									{ __( 'Cancel' ) }
								</Button>
							</FlexItem>
							<FlexItem>
								<Button variant="primary" type="submit">
									{ __( 'Save' ) }
								</Button>
							</FlexItem>
						</Flex>
					</form>
				</Modal>
			) }
		</>
	);
}
export default addToCustomBlockPatternsButton;
