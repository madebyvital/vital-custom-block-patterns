const POPUP_DISPLAY_TIME = 2000;//milliseconds

document.addEventListener('DOMContentLoaded', () => {
	const clickable = document.querySelectorAll('[data-click-to-copy]');
	if (!clickable) {
		return;
	}

	import('./popup-note.css');
});

document.addEventListener('click', (e) => {
	const clicked = e.target.closest('[data-click-to-copy]');
	if (!clicked) {
		return;
	}

	return onClick(clicked, e);
});

function onClick(clicked, e) {
	navigator.clipboard.writeText(clicked.dataset.clickToCopy);

	const popUpNote = document.createElement('div');
	popUpNote.textContent = 'Copied to Clipboard!';
	popUpNote.classList.add('popup-note');
	popUpNote.style.top = `${e.y}px`;
	popUpNote.style.left = `${e.x}px`;

	document.body.append(popUpNote);
	setTimeout(() => {
		popUpNote.classList.add('visible');
	}, 0);

	setTimeout(() => {
		popUpNote.remove();
	}, POPUP_DISPLAY_TIME);
}
