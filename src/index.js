const { registerPlugin } = wp.plugins;
import { CustomBlockPatternsMenuItems } from './components'
import './index.scss';
 
registerPlugin( 'add-to-custom-block-patterns-menu-item', {
    render: CustomBlockPatternsMenuItems,
} );